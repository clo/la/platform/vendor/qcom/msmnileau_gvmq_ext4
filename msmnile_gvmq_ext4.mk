# Inherit from the base product
$(call inherit-product, device/qcom/msmnile_gvmq/msmnile_gvmq.mk)
TARGET_BASE_PRODUCT := msmnile_gvmq


PRODUCT_NAME := msmnile_gvmq_ext4
PRODUCT_DEVICE := msmnile_gvmq_ext4
PRODUCT_BRAND := qti
PRODUCT_MODEL := msmnile_gvmq_ext4 for arm64

TARGET_OUT_INTERMEDIATES := out/target/product/$(PRODUCT_NAME)/obj
$(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr:
	mkdir -p $(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr
# Change Kernel modules install path
KERNEL_MODULES_INSTALL := dlkm
KERNEL_MODULES_OUT := out/target/product/$(PRODUCT_DEVICE)/$(KERNEL_MODULES_INSTALL)/lib/modules

TARGET_BOARD_DERIVATIVE_SUFFIX := _ext4
ENABLE_AB ?= true
ifeq ($(ENABLE_AB), true)
PRODUCT_COPY_FILES += device/qcom/msmnile_gvmq_ext4/fstab_AB_dynamic_partition_variant.gen4.qti:$(TARGET_COPY_OUT_VENDOR_RAMDISK)/first_stage_ramdisk/fstab.gen4.qcom
else
PRODUCT_COPY_FILES += device/qcom/msmnile_gvmq_ext4/fstab_non_AB_dynamic_partition_variant.gen4.qti:$(TARGET_COPY_OUT_VENDOR_RAMDISK)/first_stage_ramdisk/fstab.gen4.qcom
endif
